using System;
using System.ComponentModel.DataAnnotations;

namespace Auth.Models
{
    public class Session
    {
        public string Id { get; set; }
        [Required]
        public string SesId { get; set; }
        [Required]
        public DateTime Expiration { get; set; }

        [Required]
        public string UserId { get; set; }
        public User User { get; set; }
    }
}