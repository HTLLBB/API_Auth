using System;
using System.Security.Cryptography;

namespace Auth.Lib
{
    public static class Ses
    {
        public static string GenerateRandomSesId()
        {
            var buf = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(buf);
            }

            return BitConverter.ToString(buf).Replace("-", "");
        }
    }
}