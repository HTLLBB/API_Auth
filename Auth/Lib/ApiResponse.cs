using System.Collections.Generic;

namespace Auth.Lib
{
    public class ApiResponse : ApiResponse<string>
    {
        public ApiResponse()
        {
            Message = "";
        }
    }

    public class ApiResponse<T>
    {
        public List<string> Errors = new List<string>();
        public T Message;
        public bool Success;
    }
}