using System.Linq;
using System.Threading.Tasks;
using Auth.Controllers;
using Auth.Lib;
using Auth.Models;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Auth.Tests
{
    public class AuthControllerTests
    {
        public AuthControllerTests()
        {
            _ctx = new AuthContext(
                new DbContextOptionsBuilder<AuthContext>()
                    .UseInMemoryDatabase("AuthTests")
                    .Options
            );
        }

        private readonly AuthContext _ctx;

        [Theory]
        [InlineData("filedesless", "john@doe.com", "Letmein!")]
        public async Task IntroduceValidUserAsync(string username, string email, string password)
        {
            // Arrange
            var user = new NewUser
            {
                Username = username,
                Email = email,
                Password = password
            };

            // Act
            var res = await new AuthController(_ctx).Introduce(user);

            // Assert
            Assert.NotNull(res);
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotNull(_ctx.Users.Find(res.Message));
            Assert.Empty(res.Errors);
        }

        [Theory]
        [InlineData("", "anything", "")]
        [InlineData("anything", "", "Letmein!")]
        [InlineData("something", "", "toolamepassword")]
        [InlineData("thing", "something", "CAPSARECRUISECONTROLFORCOOL")]
        [InlineData("test", "asdf", "asdfasdfasdf1")]
        [InlineData("", "", "Letmein!")]
        public async Task IntroduceBadUserAsync(string username, string email, string password)
        {
            // Arrange
            var user = new NewUser
            {
                Username = username,
                Email = email,
                Password = password
            };

            var expectedCount = 0;
            if (string.IsNullOrWhiteSpace(username)) expectedCount++;
            if (string.IsNullOrWhiteSpace(email)) expectedCount++;
            if (password.Length < 8) expectedCount++;
            if (!password.Any(char.IsUpper)) expectedCount++;
            if (!password.Any(char.IsLower)) expectedCount++;
            if (password.All(char.IsLetter)) expectedCount++;

            // Act
            var res = await new AuthController(_ctx).Introduce(user);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotEmpty(res.Errors);
            Assert.Equal(expectedCount, res.Errors.Count);
        }

        [Theory]
        [InlineData("file", "file@hotmail.com", "xXxf1l3xXx")]
        public async Task IntroduceExistingUserAsync(string username, string email, string password)
        {
            // Arrange
            var user = new NewUser
            {
                Username = username,
                Email = email,
                Password = password
            };
            await IntroduceValidUserAsync(username, email, password);

            // Act
            var res = await new AuthController(_ctx).Introduce(user);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotEmpty(res.Errors);
            // User and email already taken
            Assert.Equal(2, res.Errors.Count);
        }

        [Fact]
        public async Task AuthenticateBadCredsAsync()
        {
            // Arrange
            var user = new KnownUser
            {
                Username = "BadCredsUser",
                Password = "Password123!"
            };
            await IntroduceValidUserAsync(user.Username, $"{user.Username}@mail.me", "SomeGoodPass127");

            // Act
            var res = await new AuthController(_ctx).Authenticate(user);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task AuthenticateInexistantUser()
        {
            // Arrange
            var user = new KnownUser
            {
                Username = "inexistantUser",
                Password = "whatever"
            };

            // Act
            var res = await new AuthController(_ctx).Authenticate(user);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task AuthenticateValidUserAsync()
        {
            // Arrange
            var user = new KnownUser
            {
                Username = "authvalid",
                Password = "SomeGoodPassword!"
            };
            await IntroduceValidUserAsync(user.Username, $"{user.Username}@email.com", user.Password);

            // Act
            var res = await new AuthController(_ctx).Authenticate(user);

            // Assert
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(_ctx.Sessions.Where(s => s.SesId == res.Message));
            Assert.Empty(res.Errors);
        }

        [Fact]
        public async Task EraseBadSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).Erase(sessionId);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task EraseValidSessionId()
        {
            // Arrange
            await IntroduceValidUserAsync("ToEraseUser", "ToEraseUser@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "ToEraseUser",
                Password = "StupidPass12"
            });
            var sessionId = authResponse.Message;

            // Act
            var res = await new AuthController(_ctx).Erase(sessionId);

            // Assert
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.Empty(_ctx.Users.Where(u => u.Id == res.Message));
            Assert.Empty(res.Errors);
        }

        [Fact]
        public async Task IdentifyBadSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).Identify(sessionId);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task IdentifyValidSessionId()
        {
            // Arrange
            await IntroduceValidUserAsync("ValidSessionId", "ValidSessionId@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "ValidSessionId",
                Password = "StupidPass12"
            });
            var sessionId = authResponse.Message;

            // Act
            var identResponse = await new AuthController(_ctx).Identify(sessionId);

            // Assert
            Assert.True(identResponse.Success);
            Assert.False(string.IsNullOrWhiteSpace(identResponse.Message));
            Assert.Single(_ctx.Users.Where(u => u.Username == identResponse.Message));
            Assert.Empty(identResponse.Errors);
        }

        [Fact]
        public async Task InvalidateBadSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).Invalidate(sessionId);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task InvalidateValidSessionId()
        {
            // Arrange
            await IntroduceValidUserAsync("Invalidate", "Invalidate@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "Invalidate",
                Password = "StupidPass12"
            });
            var sessionId = authResponse.Message;

            // Act
            var res = await new AuthController(_ctx).Invalidate(sessionId);

            // Assert
            Assert.True(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Empty(_ctx.Sessions.Where(s => s.SesId == sessionId));
            Assert.Empty(res.Errors);
        }

        [Fact]
        public async Task ResiliateBadSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).Resiliate(sessionId);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task ResilisateValidSessionId()
        {
            // Arrange
            await IntroduceValidUserAsync("Resiliate", "Resiliate@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "Resiliate",
                Password = "StupidPass12"
            });
            var sessionId = authResponse.Message;

            // Act
            var res = await new AuthController(_ctx).Resiliate(sessionId);
            var user = await _ctx.Users.FindAsync(res.Message);

            // Assert
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotNull(_ctx.Users.Find(res.Message));
            Assert.Empty(user.Sessions);
            Assert.Empty(res.Errors);
        }

        [Fact]
        public async Task RenameValidNewName()
        {
            // Arrange
            await IntroduceValidUserAsync("RenameValid", "RenameValid@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "RenameValid",
                Password = "StupidPass12"
            });
            var newName = new NewName
            {
                SessionId = authResponse.Message,
                Username = "NewNameRenameValid"
            };

            // Act
            var res = await new AuthController(_ctx).Rename(newName);

            // Assert
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotNull(_ctx.Users.Find(res.Message));
            Assert.Empty(res.Errors);
        }

        [Theory]
        [InlineData("RenameInvalidExisting")]
        [InlineData("")]
        public async Task RenameInvalidNewName(string newUsername)
        {
            // Arrange
            if (! await _ctx.Users.AnyAsync(u => u.Username == "RenameInvalid"))
                await IntroduceValidUserAsync("RenameInvalid", "RenameInvalid@email.com", "StupidPass12");
            if (! await _ctx.Users.AnyAsync(u => u.Username == "RenameInvalidExisting"))
                await IntroduceValidUserAsync("RenameInvalidExisting", "RenameInvalidExisting@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "RenameInvalid",
                Password = "StupidPass12"
            });
            var newName = new NewName
            {
                SessionId = authResponse.Message,
                Username = newUsername
            };

            // Act
            var res = await new AuthController(_ctx).Rename(newName);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotEmpty(res.Errors);
        }

        [Fact]
        public async Task RenameInvalidSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).Rename(new NewName { Username = "potato", SessionId = sessionId });

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }

        [Fact]
        public async Task ChangePassValidNewPass()
        {
            // Arrange
            await IntroduceValidUserAsync("ChangePassValid", "ChangePassValid@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "ChangePassValid",
                Password = "StupidPass12"
            });
            var newPass = new NewPass
            {
                SessionId = authResponse.Message,
                Password = "StupidPass13"
            };

            // Act
            var res = await new AuthController(_ctx).ChangePass(newPass);

            // Assert
            Assert.True(res.Success);
            Assert.False(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotNull(_ctx.Users.Find(res.Message));
            Assert.Empty(res.Errors);
        }

        [Theory]
        [InlineData("toolameofapassword")]
        [InlineData("CAPSARECRUISECONTROLFORCOOL")]
        [InlineData("tooshort")]
        [InlineData("PasswordIsSoCool")]
        [InlineData("1asdfasdfasdf")]
        public async Task ChangePassInvalidNewPass(string newPassword)
        {
            // Arrange
            if (! await _ctx.Users.AnyAsync(u => u.Username == "ChangePassInvalid"))
                await IntroduceValidUserAsync("ChangePassInvalid", "ChangePassInvalid@email.com", "StupidPass12");
            var authResponse = await new AuthController(_ctx).Authenticate(new KnownUser
            {
                Username = "ChangePassInvalid",
                Password = "StupidPass12"
            });
            var newPass = new NewPass
            {
                SessionId = authResponse.Message,
                Password = newPassword
            };

            // Act
            var res = await new AuthController(_ctx).ChangePass(newPass);

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.NotEmpty(res.Errors);
        }

        [Fact]
        public async Task ChangePassInvalidSessionId()
        {
            // Arrange
            string sessionId;
            do
            {
                sessionId = Ses.GenerateRandomSesId();
            } while (await _ctx.Sessions.AnyAsync(s => s.SesId == sessionId));

            // Act
            var res = await new AuthController(_ctx).ChangePass(new NewPass { Password = "Stupidpass11", SessionId = sessionId });

            // Assert
            Assert.False(res.Success);
            Assert.True(string.IsNullOrWhiteSpace(res.Message));
            Assert.Single(res.Errors);
        }   
    }
}