[![pipeline status](https://gitlab.com/filedesless/API_Auth/badges/master/pipeline.svg)](https://gitlab.com/filedesless/API_Auth/commits/master)

# Auth microservice

This api defines the following endpoints. They should all be accessed via POST requests, with a `Content-type: application/json` header and a json payload.

Most of these endpoints deal with a SessionId, and handle the overall User and Session management.

A SessionId is a 16 bytes (128 bits / 32 hex characters) random number generated using a CSPRNG (Cryptographically Secure Pseudo-Random Number Generator) that serves as an ephemeral authentication token for a user. Along with a persistent server state, it allows the server to maintain a confidential, ephemeral, refreshable and revokable session.

All these endpoints have a standard `ApiResponse<T>` defined (where T defaults to String), which will be returned (along an HTTP 200 OK) as a json output like:

```json
{
  "Success": true,
  "Message": "Some insightful message",
  "Errors": []
}
```

or

```json
{
  "Success": false,
  "Message": "",
  "Errors": [
    "I'm sorry",
    "I'll make sure it won't happen again"
  ]
}
```

## Auth/Introduce: NewUser -> ApiResponse

This endpoint creates a new user in the system, given an unused email, username, and strong password. It will return the new user's UserId on success, or the list of invalid parameter(s) on failure.

Here are the properties expected for a valid NewUser:

* Username:
  * Unique
  * Non empty
* Email:
  * Unique
  * Non empty
* Password:
  * At least 8 characters long
  * Contains (at least) 1 upper case character
  * Contains (at least) 1 lower case character
  * Contains (at least) 1 non letter character

Example of a valid NewUser:

```json
{
    "Username": "filedesless",
    "Email": "john@doe.com",
    "Password": "Letmein!"
}
```

## Auth/Authenticate: KnownUser -> ApiResponse

This endpoint creates a session for a known user in the system, given its username and password. It will return a new SessionId on success, or an error on failure.

Example of a valid KnownUser:

```json
{
    "Username": "filedesless",
    "Password": "Letmein!"
}
```

## Auth/Identify: SessionId -> ApiResponse

This endpoint identifies a user based on a given SessionId. It will return the user's username on success, or an error if presented an inexistant or expired SessionId.

Example of valid SessionId:

```json
"9DBCCEA472787D8E1968E9C34185A133"
```

## Auth/Self/Erase: SessionId -> ApiResponse

This endpoint deletes a user based on a given SessionId. It will return the old user's userId on success, but an error if presented an inexistant or expired SessionId.

Example of valid SessionId:

```json
"331A87274AECCBD958143C9E8691E8D7"
```

## Auth/Self/Invalidate: SessionId -> ApiResponse

This endpoint will invalidate the given SessionId. It will not return any message on success, but an error if presented an inexistant or expired SessionId.

Example of valid SessionId:

```json
"7A534BC2B0E0A3D2B6C231EB4D722F85"
```

## Auth/Self/Resiliate: SessionId -> ApiResponse

This endpoint will resiliate all the sessions associated with a user based on its given SessionId. It will return the user's userId on success, but an error if presented an inexistant or expired SessionId.

Example of valid SessionId:

```json
"B012BDD72A65854C2194428FA6A9228B"
```

## Auth/Self/Rename: NewName -> ApiResponse

This endpoint will change the username of the user associated with the given SessionId. It will return the user's userId on success, but an error if presented an inexistant or expired SessionId. Same rules as Auth/Introduce apply for the username.

Example of valid NewName:

```json
{
  "SessionId": "B4342909941967E8E137AD6E7BF8250B",
  "Username": "filedesmore"
}
```

## Auth/Self/ChangePass: NewPass -> ApiResponse

This endpoint will change the password of the user associated with the given SessionId. It will return the user's userId on success, but an error if presented an inexistant or expired SessionId. Same rules as Auth/Introduce apply for the password.

Example of valid NewPass:

```json
{
  "SessionId": "B0528FB7E6DA731E8E7691499092434B",
  "Password": "Some1337Password"
}
```